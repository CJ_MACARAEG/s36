const express = require ('express')
const mongoose = require ('mongoose')
const dotenv = require ('dotenv')
const taskRoute = require('./routes/taskRoutes')

// Initialize dotenv
dotenv.config()

// Server setup
const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Connection
mongoose.connect(`mongodb+srv://cjmacaraeg900:${process.env.MONGODB_PASSWORD}@cluster0.it74nuq.mongodb.net/S36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error('Connection error'))
db.on('open', () => console.log('Welcome to MongoDB!'))

//Routes
app.use('/tasks', taskRoute)


// Server Listening
app.listen(port, () => console.log(`Server running on localhost:${port}`))


// si Routes taga call ng process. 
// si Controllers taga gawa ng process 