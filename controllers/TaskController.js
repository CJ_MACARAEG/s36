const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask 
	})
}

module.exports.getAllTask = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if (error) {
			console.log(error)
			return error
		}

		result.name = new_data.name 

		return result.save().then((updatedTask, error) => {
			if (error) {
				console.log(error)
				return error
			}
			
			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id) => {
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
		if(error){
			console.log(error)
			return error
		}

		return removedTask
	})
}

// =================  ACTIVITY SESSION ===============================

// get Specific Task
module.exports.getSpecificTask = () => {
	return Task.findOne({}).then((result) => {
		return result
	})
}

// update Specific Task
module.exports.updateSpecificTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if (error) {
			console.log(error)
			return error
		}

		result.status = new_data.status 

		return result.save().then((updatedTask, error) => {
			if (error) {
				console.log(error)
				return error
			}
			
			return updatedTask
		})
	})
}